<?php

$persons = array(
"Birger Schlaug",
"Alexandra Pascalidou",
"Zlatan",
"Soran Ismail",
"Loreen",
"Andreas Cervenka",
"Laleh",
"Gustav Fridolin",
"Lars Leijonborg",
"Stefan Löfven",
"Fredrik Reinfeldt",
"Mona Sahlin",

);

$adverbs = array(
	"totalt",
	"komplett",
	"tillfälligt",
	"abrupt",
	"abstrakt",
	"autonomt",
	"bajsigt",
	"bakslugt",
	"banalt",
	"argsint",
	"arrogant",
	"flexibelt",
	"håglöst",
	"ideologiskt",
	"konstitutionellt",
	"lovgirit",

);


$verbs = array(
	'jämnställer',
	'demonstrerar',
	'likriktar',
	'påvisar',
	'sexualiserar',
	'fotobombar',
	'googlekör',
	'genusplogar',
	'karriärcoachar',
	'kompetensväxlar',
	'mervärdesmatar',
	'tårtar',
	'mobilblottar',
	'reklejmar',
	'pudlar',
	'kemkastrerar',
	'social-turistar'
);

$adjectives = array(
	"grön",
	"jämställd",
	"transsexuell",
	"metrosexuell",
	"bloggande",
	"plusjobbande",
	"instegsjobbande",
	"SFIande"
);

$nouns = array(
	"kosheria",
	"transperson",
	"hen",
	"snålsurfare",
	"kostymrasist",
	"rugbyförälder",
	"neger",
	"rasifierad-afrikan",
	"arab"
);
/*
$verbs = array(
	"aggregate",
	"architect",
	"benchmark",
	"brand",
	"cultivate",
	"deliver",
	"deploy",
	"disintermediate",
	"drive",
	"e-enable",
	"embrace",
	"empower",
	"enable",
	"engage",
	"engineer"
);




$adjectives = array(
	"24/365",
	"24/7",
	"B2B",
	"B2C",
	"back-end",
	"best-of-breed",
	"bleeding-edge",
	"bricks-and-clicks",
	"clicks-and-mortar",
	"collaborative",
	"compelling",
	"cross-platform",
	"cross-media"
);

$nouns = array(
	"action-items",
	"applications",
	"architectures",
	"bandwidth",
	"channels",
	"communities",
	"content",
	"convergence",
	"deliverables",
	"e-business",
	"e-commerce",
	"e-markets",
	"e-services",
	"e-tailers",
	"experiences"
);
*/
## adverb är svåra, strunta i dem sålänge

echo $persons[rand(0,count($persons)-1)]." ".$adverbs[rand(0,count($adverbs)-1)]." ".$verbs[rand(0,count($verbs)-1)]." ".$adjectives[rand(0,count($adjectives)-1)]." ".$nouns[rand(0,count($nouns)-1)]."\r\n";